/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const int startwithgaps	     = 1;	 /* 1 means gaps are used by default */
static const unsigned int gappx     = 10;       /* default gap between windows in pixels */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft  = 0;   /* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;        /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int user_bh            = 2;        /* 2 is the default spacing around the bar's font */
#define ICONSIZE 18   /* icon size */
#define ICONSPACING 5 /* space between icon and title */
static const char *fonts[]          = { "Liberation:size=11:antialias:true", "FontAwesome6Free:style=Solid:size=12:antialias:true",
                                        "FontAwesome6Brands:style=Regular:size=12:antialias:true" };
static const char col_grey1[]       = "#1D1B28";
static const char col_grey2[]       = "#333333";
static const char col_grey3[]       = "#B8B8B8";
static const char col_grey4[]       = "#EEEEEE";
static const char col_white[]       = "#FFFFFF";
static const char col_cyan[]        = "#00FEC5";
static const char col_blue[]        = "#4462F5";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_grey3, col_grey1, col_grey2 },
	[SchemeSel]  = { col_white, col_cyan,  col_blue  },
  /*[SchemeStatus]  = { col_grey1, col_grey1,  "#000000"  }, // Statusbar right {text,background,not used but cannot be empty} */	
	[SchemeTagsNorm]  = { col_grey3, col_grey1,  "#000000"  }, // Tagbar left unselected {text,background,not used but cannot be empty}
  [SchemeTagsSel]  = { col_white, col_blue,  "#000000"  }, // Tagbar left selected {text,background,not used but cannot be empty}                                                    	
	[SchemeInfoNorm]  = { col_grey3, col_grey1,  "#000000"  }, // infobar middle  unselected {text,background,not used but cannot be empty}
  [SchemeInfoSel]  = { col_white, col_grey1,  "#000000"  }, // infobar middle  selected {text,background,not used but cannot be empty}                                                      //
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
	/* { "", NULL,       NULL,       1 << 8,            0,           -1 }, */
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 0; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */	
  { "TT",      bstack },
	{ "==",      bstackhoriz },
  { "[M]",      monocle },
  { "><>",      NULL },    /* no layout function means floating behavior */
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-c", "-l", "18", NULL };
static const char *rofi[]     = { "rofi", "-show", "drun", NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *upvol[]    = { "amixer", "-q", "set", "Master", "2%+", "unmute", NULL };
static const char *downvol[]  = { "amixer", "-q", "set", "Master", "2%-", NULL };
static const char *mutevol[]  = { "amixer", "-q", "set", "Master", "toggle", NULL };
static const char *uplight[]  = { "light", "-A", "1", NULL };
static const char *downlight[] = { "light", "-U", "1", NULL };
static const char *screenshot[] = { "scrot", "-F", "%b%d:%H:%M.%S.png", NULL };
static const char *lockscreen[] = { "slock", NULL };
static const char *surfcmd[] = { "tabbed", "surf", "-e", NULL };
static const char *redshift[] = { "redshift", "-o", NULL };

static const Key keys[] = {
	/* modifier                     key        function        argument */
  { 0,                 XF86XK_AudioMute,     spawn,          {.v = mutevol } },
  { 0,          XF86XK_AudioLowerVolume,     spawn,          {.v = downvol } },
  { 0,          XF86XK_AudioRaiseVolume,     spawn,          {.v = upvol } },
  { 0,           XF86XK_MonBrightnessUp,     spawn,          {.v = uplight } },
  { 0,         XF86XK_MonBrightnessDown,     spawn,          {.v = downlight } },
  { MODKEY,                       XK_x,      spawn,          {.v = screenshot } },
  { MODKEY|ControlMask,           XK_Return, spawn,          {.v = lockscreen } },
	{ MODKEY|ControlMask,           XK_f,      spawn,          {.v = dmenucmd } },
  { MODKEY,                       XK_f,      spawn,          {.v = rofi } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
  { MODKEY,                       XK_s,      spawn,          {.v = surfcmd } },
  { MODKEY|ShiftMask,             XK_r,      spawn,          {.v = redshift } },
  { MODKEY|ShiftMask,             XK_f,      togglefullscreen, {0} },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY|ShiftMask,             XK_j,      rotatestack,    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      rotatestack,    {.i = -1 } },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_space,  cyclelayout,    {.i = +1 } },
	{ MODKEY,                       XK_t,      togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -5 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +5 } },
	{ MODKEY|ShiftMask,             XK_minus,  setgaps,        {.i = GAP_RESET } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = GAP_TOGGLE} },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ControlMask,           XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        cyclelayout,    {.i = +1 } }, 
	{ ClkLtSymbol,          0,              Button3,        cyclelayout,    {.i = -1 } }, 
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
